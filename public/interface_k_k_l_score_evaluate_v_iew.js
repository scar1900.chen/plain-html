var interface_k_k_l_score_evaluate_v_iew =
[
    [ "initWithFrame:numberOfStars:isVariable:", "interface_k_k_l_score_evaluate_v_iew.html#a4e41066e3982addf83d19c2e2affea7a", null ],
    [ "setDefaultScore:", "interface_k_k_l_score_evaluate_v_iew.html#ae24c79085311df8d9e0bdc1d6732cc4c", null ],
    [ "actualScore", "interface_k_k_l_score_evaluate_v_iew.html#a8910cba291a0946f0c1c78cc5fe5af06", null ],
    [ "fullScore", "interface_k_k_l_score_evaluate_v_iew.html#a0d356e4bf3a84671fde8c7026455a94b", null ],
    [ "isContrainsHalfStar", "interface_k_k_l_score_evaluate_v_iew.html#aca195195da7d47ea1e966f241a608d47", null ],
    [ "scoreBlock", "interface_k_k_l_score_evaluate_v_iew.html#aa426e085de95163b528e70a1d1bb7b10", null ]
];