var category_x_h_x_tab_item_07_08 =
[
    [ "badgeLabel", "category_x_h_x_tab_item_07_08.html#aa3307a558eca5631175ec7c8d7d30279", null ],
    [ "dotBadgeCenterMarginRight", "category_x_h_x_tab_item_07_08.html#a24890c854841b0d0d4abe3501f1e8c72", null ],
    [ "dotBadgeMarginTop", "category_x_h_x_tab_item_07_08.html#ab15abd16c3f1e7359447cf31c8dcbd75", null ],
    [ "dotBadgeSideLength", "category_x_h_x_tab_item_07_08.html#ae669e0ff63a791ca577c8011bf5a8a56", null ],
    [ "numberBadgeCenterMarginRight", "category_x_h_x_tab_item_07_08.html#a5298d3ac944129cf4fe2aac33cd062b5", null ],
    [ "numberBadgeMarginTop", "category_x_h_x_tab_item_07_08.html#a674381d009e72e33b4e977560a6e7081", null ],
    [ "numberBadgeTitleHorizonalSpace", "category_x_h_x_tab_item_07_08.html#ae722e84c4f25e72b330cfc865a5f5c5d", null ],
    [ "numberBadgeTitleVerticalSpace", "category_x_h_x_tab_item_07_08.html#a706d8e2499d3db442eb6895f2bba2fe0", null ],
    [ "redDotView", "category_x_h_x_tab_item_07_08.html#a7ef253e175c7510b07ed0399c0f214b6", null ],
    [ "spacing", "category_x_h_x_tab_item_07_08.html#ad54412001a063c7bd9561fd678e51117", null ],
    [ "verticalOffset", "category_x_h_x_tab_item_07_08.html#aa52674ec3dbbd9e0c8be2c30b03c536f", null ]
];