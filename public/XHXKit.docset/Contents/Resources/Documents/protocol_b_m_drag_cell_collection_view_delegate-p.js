var protocol_b_m_drag_cell_collection_view_delegate_p =
[
    [ "dragCellCollectionView:beganDragAtPoint:indexPath:", "protocol_b_m_drag_cell_collection_view_delegate-p.html#a4a5fad8a5de13e783afe2e6418ccb76a", null ],
    [ "dragCellCollectionView:changedDragAtPoint:indexPath:", "protocol_b_m_drag_cell_collection_view_delegate-p.html#aca75a2cdad684df5dc2ed0991ef275a1", null ],
    [ "dragCellCollectionView:endedDragAtPoint:indexPath:", "protocol_b_m_drag_cell_collection_view_delegate-p.html#a5add2eca826f05f64856ec7b7d49522d", null ],
    [ "dragCellCollectionView:endedDragAutomaticOperationAtPoint:section:indexPath:", "protocol_b_m_drag_cell_collection_view_delegate-p.html#a175791f74ad07e0d2147cd4ef1adbb02", null ],
    [ "dragCellCollectionView:willDeleteCellAtIndexPath:", "protocol_b_m_drag_cell_collection_view_delegate-p.html#aa7a3c2d0e80b95549e3298817f6915fd", null ],
    [ "dragCellCollectionView:willMoveCellFormIndexPath:toIndexPath:", "protocol_b_m_drag_cell_collection_view_delegate-p.html#a275dc987fda1fd9daa2b41612575d85c", null ],
    [ "dragCellCollectionViewDidEndDrag:", "protocol_b_m_drag_cell_collection_view_delegate-p.html#aa1695e04ca12377062a397acd95fec1b", null ],
    [ "dragCellCollectionViewShouldBeginExchange:sourceIndexPath:toIndexPath:", "protocol_b_m_drag_cell_collection_view_delegate-p.html#aab714e7200848ed9adc858f31929fe72", null ],
    [ "dragCellCollectionViewShouldBeginMove:indexPath:", "protocol_b_m_drag_cell_collection_view_delegate-p.html#ae709bb53811434faa30df49142869d10", null ]
];