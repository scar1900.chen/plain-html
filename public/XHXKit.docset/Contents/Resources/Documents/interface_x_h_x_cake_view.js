var interface_x_h_x_cake_view =
[
    [ "addCake:", "interface_x_h_x_cake_view.html#a25403316cb33317c8f6223ff09bcc443", null ],
    [ "setupProgress:values:", "interface_x_h_x_cake_view.html#a66f385b2cfb86077add347e96064ff75", null ],
    [ "cakes", "interface_x_h_x_cake_view.html#a31f2da6515b8dfd61aa2b20dff73c373", null ],
    [ "cakeSpace", "interface_x_h_x_cake_view.html#a8d63b8bb62778da52983a90d70eb6839", null ],
    [ "innerRadius", "interface_x_h_x_cake_view.html#a761f5c7901a07a750838d99179ca01c0", null ],
    [ "selectedIndex", "interface_x_h_x_cake_view.html#a0dd5b2485f0f9a1202dc47946f500276", null ],
    [ "selectItemBlk", "interface_x_h_x_cake_view.html#a2e7da70d1f1d345156f000f5fba91bb9", null ],
    [ "totalRadius", "interface_x_h_x_cake_view.html#af8b551290aa68488aaed49b02a20cc3a", null ],
    [ "type", "interface_x_h_x_cake_view.html#a31993300e2a1eabbeec1e0444c9ad64e", null ]
];