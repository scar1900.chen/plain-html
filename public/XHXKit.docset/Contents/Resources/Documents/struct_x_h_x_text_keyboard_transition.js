var struct_x_h_x_text_keyboard_transition =
[
    [ "animationCurve", "struct_x_h_x_text_keyboard_transition.html#afacd2636b7e1409a917dad69d10a06e1", null ],
    [ "animationDuration", "struct_x_h_x_text_keyboard_transition.html#a6d215ca6c6f122a9a0af0e7b48040693", null ],
    [ "animationOption", "struct_x_h_x_text_keyboard_transition.html#a1f968b27988feec878f28406635023fe", null ],
    [ "fromFrame", "struct_x_h_x_text_keyboard_transition.html#aad15905ae428fe9bfdfad2010087b680", null ],
    [ "fromVisible", "struct_x_h_x_text_keyboard_transition.html#a0b00535442714dc6ccf8e68a34fb91f3", null ],
    [ "toFrame", "struct_x_h_x_text_keyboard_transition.html#a97527530fdc984218af25924c085e936", null ],
    [ "toVisible", "struct_x_h_x_text_keyboard_transition.html#af664f9a3d1c9aa90b635f6c94c383679", null ]
];