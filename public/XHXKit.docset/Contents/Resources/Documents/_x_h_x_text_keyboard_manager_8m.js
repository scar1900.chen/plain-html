var _x_h_x_text_keyboard_manager_8m =
[
    [ "_XHXTextKeyboardViewFrameObserver", "interface___x_h_x_text_keyboard_view_frame_observer.html", "interface___x_h_x_text_keyboard_view_frame_observer" ],
    [ "XHXTextIsAppExtension", "_x_h_x_text_keyboard_manager_8m.html#ad62ac520d11f2b28c8607820afd9065c", null ],
    [ "XHXTextSharedApplication", "_x_h_x_text_keyboard_manager_8m.html#a7b31e4b1b1d2e323054fe61a1088a6d0", null ],
    [ "_fromFrame", "_x_h_x_text_keyboard_manager_8m.html#a5c42c49216d0199c2ec23de072e7f86e", null ],
    [ "_fromOrientation", "_x_h_x_text_keyboard_manager_8m.html#a65b6248ffb057531f1fe9247a86b5271", null ],
    [ "_fromVisible", "_x_h_x_text_keyboard_manager_8m.html#a4e25d7403955cf4a163763ac0ea282f5", null ],
    [ "_hasNotification", "_x_h_x_text_keyboard_manager_8m.html#ad0428514766fa4ba70e4e1929335f5dc", null ],
    [ "_hasObservedChange", "_x_h_x_text_keyboard_manager_8m.html#abccee6ddba6ef7510ebbe63ba3328176", null ],
    [ "_lastIsNotification", "_x_h_x_text_keyboard_manager_8m.html#afa1e10908bab765097e7bc6686b28c40", null ],
    [ "_notificationCurve", "_x_h_x_text_keyboard_manager_8m.html#a681534c5fb7511e7ade4254052500c3c", null ],
    [ "_notificationDuration", "_x_h_x_text_keyboard_manager_8m.html#a2e05d81fbbdd32dbbfc787a371ef65f7", null ],
    [ "_notificationFromFrame", "_x_h_x_text_keyboard_manager_8m.html#ab6da2fc00222a11ccead764ebdaed82f", null ],
    [ "_notificationToFrame", "_x_h_x_text_keyboard_manager_8m.html#afa77abf0d466d4f9aa360c7c3cddc19a", null ],
    [ "_observedToFrame", "_x_h_x_text_keyboard_manager_8m.html#a49525ed949ab3c5a97809f1a8dcbb563", null ]
];