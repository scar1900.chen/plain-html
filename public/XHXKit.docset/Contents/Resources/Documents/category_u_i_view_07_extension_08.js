var category_u_i_view_07_extension_08 =
[
    [ "centerX", "category_u_i_view_07_extension_08.html#a6285cd1f00ae968e8a3bc968b9310c66", null ],
    [ "centerY", "category_u_i_view_07_extension_08.html#af11c04e661b1bc41a679bb4a3a5973c1", null ],
    [ "height", "category_u_i_view_07_extension_08.html#aa3b3e41eef35585adfa16b596c4cbbbf", null ],
    [ "maxX", "category_u_i_view_07_extension_08.html#afc617b816d5148ffc58f21b104f07c5d", null ],
    [ "maxY", "category_u_i_view_07_extension_08.html#ac146a5084a23be0cc0550279c3a6e53c", null ],
    [ "minX", "category_u_i_view_07_extension_08.html#af5b33deb855b93f66f476d335f1748f1", null ],
    [ "minY", "category_u_i_view_07_extension_08.html#ab27fb788fadeb520e777e5d07f4a3737", null ],
    [ "origin", "category_u_i_view_07_extension_08.html#a3977eaa869e83b624757582c1c682060", null ],
    [ "size", "category_u_i_view_07_extension_08.html#ac1bdf5b4716f30dde17db5eda5d9ca71", null ],
    [ "width", "category_u_i_view_07_extension_08.html#ab4539ee3d2d7b60f1ebeed516290f7fa", null ],
    [ "x", "category_u_i_view_07_extension_08.html#a4667b0159f5c49e85a5e391fd8783aba", null ],
    [ "y", "category_u_i_view_07_extension_08.html#a2f7c5e1f7c3205789532ea1123026022", null ]
];