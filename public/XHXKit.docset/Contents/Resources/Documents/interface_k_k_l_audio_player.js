var interface_k_k_l_audio_player =
[
    [ "pause", "interface_k_k_l_audio_player.html#ab8f66c583ae6f4c3865fc3a3f7e63562", null ],
    [ "play", "interface_k_k_l_audio_player.html#ac64ebf0b646d70c086efbef451da5ea8", null ],
    [ "replayAudio:", "interface_k_k_l_audio_player.html#a7de62b429cdf4f82c0aecc7f5d9ec8b2", null ],
    [ "replayAudio:withStartTime:", "interface_k_k_l_audio_player.html#ad897c2ee7c06806747eb7b490407f55f", null ],
    [ "reset", "interface_k_k_l_audio_player.html#a54e74db0c308b1b8e03200c77bd4db64", null ],
    [ "seekToTime:", "interface_k_k_l_audio_player.html#a3a98a1dadd21b13b84ab7a8c65700cca", null ],
    [ "audio", "interface_k_k_l_audio_player.html#a5de997ab0e6131b1af5b6e1876146eaa", null ],
    [ "currentTime", "interface_k_k_l_audio_player.html#a032b079080271b43492dbefa93eb8147", null ],
    [ "error", "interface_k_k_l_audio_player.html#aa2b5e6476469b1ae714b94fd7646d369", null ],
    [ "shouldUpdatePlayerWidget", "interface_k_k_l_audio_player.html#a6b1cfeef11def971bf6e34059ea4a4ca", null ],
    [ "status", "interface_k_k_l_audio_player.html#a93c9cceea19c110cc944eb278aceaee2", null ],
    [ "totalTime", "interface_k_k_l_audio_player.html#a530cc524d9a02634421aaad0d565e0ed", null ],
    [ "volume", "interface_k_k_l_audio_player.html#a38c6dc27351f467d3a8fac39618b991f", null ]
];