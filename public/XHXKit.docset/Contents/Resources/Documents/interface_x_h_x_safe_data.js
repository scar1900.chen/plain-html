var interface_x_h_x_safe_data =
[
    [ "initWithDataArray:", "interface_x_h_x_safe_data.html#ab7d1019317c7ce8b0cbf2328affdaf24", null ],
    [ "insertItem:atIndex:refreshUI:", "interface_x_h_x_safe_data.html#ae83a338d9ebd697a0f9b93ab653f62a5", null ],
    [ "insertItem:refreshUI:", "interface_x_h_x_safe_data.html#abdb228de4960b8500866da72262fe210", null ],
    [ "insertItems:atIndexs:refreshUI:", "interface_x_h_x_safe_data.html#a7d379aed91719926c0807adaca366d27", null ],
    [ "insertItems:refreshUI:", "interface_x_h_x_safe_data.html#a77f22d947a4d2ca61ef87aba889d2f9e", null ],
    [ "moveItemAtIndex:toIndex:refreshUI:", "interface_x_h_x_safe_data.html#a3d38dec500e7dd89fd6f211778b01980", null ],
    [ "removeItemAtIndex:refreshUI:", "interface_x_h_x_safe_data.html#a11f45fa42d09c23026067b8f1394e981", null ],
    [ "removeItemsAtIndexs:refreshUI:", "interface_x_h_x_safe_data.html#ad2a6b5029c69fd341ac43a7fadc5eb22", null ],
    [ "resetDataArray:refreshUI:", "interface_x_h_x_safe_data.html#a48fcde35d929039c31b88e800d6ed68f", null ],
    [ "delegate", "interface_x_h_x_safe_data.html#ac9670bffc7b63952216a3e3be714c876", null ]
];