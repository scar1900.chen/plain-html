var dir_9c9511d05e8bec32aa25169884f5dd22 =
[
    [ "XHXThreadSafeArray.h", "_x_h_x_thread_safe_array_8h.html", [
      [ "XHXThreadSafeArray", "interface_x_h_x_thread_safe_array.html", null ],
      [ "NSArray(XHXSafe)", "category_n_s_array_07_x_h_x_safe_08.html", "category_n_s_array_07_x_h_x_safe_08" ],
      [ "NSMutableArray(XHXSafe)", "category_n_s_mutable_array_07_x_h_x_safe_08.html", "category_n_s_mutable_array_07_x_h_x_safe_08" ]
    ] ],
    [ "XHXThreadSafeArray.m", "_x_h_x_thread_safe_array_8m.html", "_x_h_x_thread_safe_array_8m" ],
    [ "XHXThreadSafeDictionary.h", "_x_h_x_thread_safe_dictionary_8h.html", [
      [ "XHXThreadSafeDictionary", "interface_x_h_x_thread_safe_dictionary.html", "interface_x_h_x_thread_safe_dictionary" ],
      [ "NSMutableDictionary(XHXSafe)", "category_n_s_mutable_dictionary_07_x_h_x_safe_08.html", "category_n_s_mutable_dictionary_07_x_h_x_safe_08" ]
    ] ],
    [ "XHXThreadSafeDictionary.m", "_x_h_x_thread_safe_dictionary_8m.html", "_x_h_x_thread_safe_dictionary_8m" ],
    [ "XHXWeakProxy.h", "_x_h_x_weak_proxy_8h.html", [
      [ "XHXWeakProxy", "interface_x_h_x_weak_proxy.html", "interface_x_h_x_weak_proxy" ]
    ] ],
    [ "XHXWeakProxy.m", "_x_h_x_weak_proxy_8m.html", null ]
];