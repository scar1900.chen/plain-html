var _x_h_x_u_r_l_config_8h =
[
    [ "XHXURLConfig", "interface_x_h_x_u_r_l_config.html", null ],
    [ "XHXGlobalApiUrl", "_x_h_x_u_r_l_config_8h.html#a314585ac4fc8980fd8f431a0eb093378", null ],
    [ "XHXH5ChekHomeworkReporUrl", "_x_h_x_u_r_l_config_8h.html#a4a576dcac4c5652f29f2e911ee9bb710", null ],
    [ "XHXH5DoHomeworkurl", "_x_h_x_u_r_l_config_8h.html#af08c246b7fb25a7c2152f61edf9550d3", null ],
    [ "XHXH5ParentMonitorGuideUrl", "_x_h_x_u_r_l_config_8h.html#ae8c377c9f97f69c3518ed97667e277e3", null ],
    [ "XHXH5URDownload", "_x_h_x_u_r_l_config_8h.html#a924676c517f0373744606cd5bed88d9a", null ],
    [ "XHXH5URLAboutUs", "_x_h_x_u_r_l_config_8h.html#a0732a266fcf26dae5850cb67029927b8", null ],
    [ "XHXH5URLAchievementShare", "_x_h_x_u_r_l_config_8h.html#aca886be99649d6b11a9cb3b6f4714705", null ],
    [ "XHXH5URLCertificateDetail", "_x_h_x_u_r_l_config_8h.html#aeaa61248cfa7b71c7631fc14eefe2fcb", null ],
    [ "XHXH5URLClassicQuestion", "_x_h_x_u_r_l_config_8h.html#a91f0feb8aca6db28a5ac9f7291c13343", null ],
    [ "XHXH5URLCombinedCourseDetail", "_x_h_x_u_r_l_config_8h.html#a905e59630153dbf4a431adad2f376d95", null ],
    [ "XHXH5URLConfirmOrder", "_x_h_x_u_r_l_config_8h.html#a405dbed282e377a3459021b1d61519fd", null ],
    [ "XHXH5URLCourse", "_x_h_x_u_r_l_config_8h.html#a423e42c32aacccab94df1c1a5ba808b2", null ],
    [ "XHXH5URLearReport", "_x_h_x_u_r_l_config_8h.html#a73591a3c73ad753c53c0a5cc7057b339", null ],
    [ "XHXH5URLErrorBook", "_x_h_x_u_r_l_config_8h.html#a931580dc0bfcbbd5cbbb413c18479d0e", null ],
    [ "XHXH5URLessonsSum", "_x_h_x_u_r_l_config_8h.html#a0e3b2918d4d49649e9bfd5c77039e455", null ],
    [ "XHXH5URLHomework", "_x_h_x_u_r_l_config_8h.html#ad87b133218de96cd93b8e6c1eeef495c", null ],
    [ "XHXH5URLLearningReport", "_x_h_x_u_r_l_config_8h.html#a3b1932358beac6f480c8cafe36847af5", null ],
    [ "XHXH5URLLiveDetail", "_x_h_x_u_r_l_config_8h.html#ae2003995bc01c7ec4008d8a302d5f340", null ],
    [ "XHXH5URLPaySuccess", "_x_h_x_u_r_l_config_8h.html#a7e68dbd18d8d88f5ea6dfb68256233ef", null ],
    [ "XHXH5URLPOINT", "_x_h_x_u_r_l_config_8h.html#ad7a159f086a7f2710516542983bde543", null ],
    [ "XHXH5URLProductCourseDetail", "_x_h_x_u_r_l_config_8h.html#ad7457db04076cbfa421a302307f8f0b3", null ],
    [ "XHXH5URLRank", "_x_h_x_u_r_l_config_8h.html#a8b6f86e2308e122a8bb1640c1e4d9b67", null ],
    [ "XHXH5URLReport", "_x_h_x_u_r_l_config_8h.html#a9efddd9773b9087ec6ba6c7eeb0b3b17", null ],
    [ "XHXH5URLShareYearReport", "_x_h_x_u_r_l_config_8h.html#a3f18cd7c37c828647f71ef6b2134f179", null ],
    [ "XHXH5URLSummary", "_x_h_x_u_r_l_config_8h.html#a95149edd1092dc3ea4c85e54ddd876f6", null ],
    [ "XHXH5URLTeacher", "_x_h_x_u_r_l_config_8h.html#afd2887bdd1f11969f7f6daa6bd3f90c1", null ],
    [ "XHXH5URLUserCenterExistActivity", "_x_h_x_u_r_l_config_8h.html#a00c90020373c5a6474d05c507527847c", null ],
    [ "XHXH5URLUserCenterOrder", "_x_h_x_u_r_l_config_8h.html#a17a7191c4b81bc7b88c9f4686a2f9031", null ],
    [ "XHXH5URLUserExchangeCourseCenter", "_x_h_x_u_r_l_config_8h.html#acca2419ff0f6db36e4e950a6c1c7ef0c", null ],
    [ "XHXH5URLUserProtocol", "_x_h_x_u_r_l_config_8h.html#a505186b10f32b64bd7b1b5ef4dd4030a", null ],
    [ "XHXH5URLUserThirdCustomer", "_x_h_x_u_r_l_config_8h.html#a9add7533e536a1301adc329d675446c7", null ],
    [ "XHXH5URLWeekReport", "_x_h_x_u_r_l_config_8h.html#a023d22e6837fc739f4d59d9bc411ff8d", null ],
    [ "XHXSpeedMonitorNeedDownload", "_x_h_x_u_r_l_config_8h.html#a5f0f8be602d75ae9fc71b489b2e6405f", null ],
    [ "XHXWebSocketURL", "_x_h_x_u_r_l_config_8h.html#a7de0e1eda6ac494d9344e813731c02c2", null ],
    [ "XHXXSCMode", "_x_h_x_u_r_l_config_8h.html#a4b2d04f5ad2adab17d9df12a0569fbb3", null ]
];