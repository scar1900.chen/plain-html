var searchData=
[
  ['panelview_2004',['panelView',['../interface_x_h_x_picker_view.html#aff723421f2629a7f2f7b7965c3edfa83',1,'XHXPickerView']]],
  ['picked_2005',['picked',['../interface_x_h_x_cake_item_view.html#afe590dd568714d72cec84198721e48a4',1,'XHXCakeItemView']]],
  ['pickerview_2006',['pickerView',['../category_x_h_x_picker_view_07_08.html#abd4c4dc6d2d97b1f7d3b822890167322',1,'XHXPickerView()']]],
  ['placeholder_2007',['placeHolder',['../category_u_i_text_view_07_x_h_x_placeholder_08.html#abba4b1b9727800363754d42b59b60955',1,'UITextView(XHXPlaceholder)']]],
  ['placeholdercolor_2008',['placeHolderColor',['../category_u_i_text_view_07_x_h_x_placeholder_08.html#a0e9bf1b3fb7ec2f1658436902b216a74',1,'UITextView(XHXPlaceholder)']]],
  ['placeholderfont_2009',['placeHolderFont',['../category_u_i_text_view_07_x_h_x_placeholder_08.html#a9ac0ae18ae67e725108938f93157da3d',1,'UITextView(XHXPlaceholder)']]],
  ['placeholderlabel_2010',['placeHolderLabel',['../category_u_i_text_view_07_x_h_x_placeholder_08.html#aaf17b4ee4516c45e6b04073a6df23964',1,'UITextView(XHXPlaceholder)']]],
  ['player_2011',['player',['../category_k_k_l_audio_player_07_08.html#a6284778beef2a2d389581d89a17269d6',1,'KKLAudioPlayer()']]],
  ['progress_2012',['progress',['../interface_x_h_x_cake.html#ad2f75de3764eff1785423eebc9b9b8ec',1,'XHXCake']]]
];
