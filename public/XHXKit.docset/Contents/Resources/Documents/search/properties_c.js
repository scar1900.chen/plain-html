var searchData=
[
  ['name_1991',['name',['../interface_x_h_x_cake.html#a1939721fae44bd2341293649ed403c80',1,'XHXCake']]],
  ['namelabel_1992',['nameLabel',['../interface_x_h_x_cake_item_view.html#ad5e0dd074d5643c5eedfb34a91dcc93d',1,'XHXCakeItemView']]],
  ['notifyblock_1993',['notifyBlock',['../interface___x_h_x_text_keyboard_view_frame_observer.html#a595de510eb2e5218fc4f387e8a7df2a0',1,'_XHXTextKeyboardViewFrameObserver']]],
  ['numberbadgecentermarginright_1994',['numberBadgeCenterMarginRight',['../category_x_h_x_tabbar_07_08.html#a401d1f88047255fdb93b00bb8c75057a',1,'XHXTabbar()::numberBadgeCenterMarginRight()'],['../category_x_h_x_tab_item_07_08.html#a5298d3ac944129cf4fe2aac33cd062b5',1,'XHXTabItem()::numberBadgeCenterMarginRight()']]],
  ['numberbadgemargintop_1995',['numberBadgeMarginTop',['../category_x_h_x_tabbar_07_08.html#aa72beb68e01fad3daa1cf3621bb5808f',1,'XHXTabbar()::numberBadgeMarginTop()'],['../category_x_h_x_tab_item_07_08.html#a674381d009e72e33b4e977560a6e7081',1,'XHXTabItem()::numberBadgeMarginTop()']]],
  ['numberbadgetitlehorizonalspace_1996',['numberBadgeTitleHorizonalSpace',['../category_x_h_x_tabbar_07_08.html#aa1bc4d84f80abcd5192ebf2d2892d190',1,'XHXTabbar()::numberBadgeTitleHorizonalSpace()'],['../category_x_h_x_tab_item_07_08.html#ae722e84c4f25e72b330cfc865a5f5c5d',1,'XHXTabItem()::numberBadgeTitleHorizonalSpace()']]],
  ['numberbadgetitleverticalspace_1997',['numberBadgeTitleVerticalSpace',['../category_x_h_x_tabbar_07_08.html#addb8521510139283630145af48b788d7',1,'XHXTabbar()::numberBadgeTitleVerticalSpace()'],['../category_x_h_x_tab_item_07_08.html#a706d8e2499d3db442eb6895f2bba2fe0',1,'XHXTabItem()::numberBadgeTitleVerticalSpace()']]],
  ['numberofcomponents_1998',['numberOfComponents',['../interface_x_h_x_picker_view.html#a4f0695fe938133c560b897a53652bcb6',1,'XHXPickerView']]],
  ['numberofstars_1999',['numberOfStars',['../category_k_k_l_score_evaluate_v_iew_07_08.html#aee694544c2bc384ce105bc77b6b1e869',1,'KKLScoreEvaluateVIew()']]]
];
