var searchData=
[
  ['getanimationbundlewithname_3a_1490',['getAnimationBundleWithName:',['../interface_x_h_x_kit_assets.html#aa96da6969c2768d5c0d218007f269036',1,'XHXKitAssets']]],
  ['getcarriername_1491',['getCarrierName',['../interface_x_h_x_device_manager.html#a057bb4ff80167d779a09dca7ae998a4a',1,'XHXDeviceManager']]],
  ['getdevicemodelname_1492',['getDeviceModelName',['../interface_x_h_x_device_manager.html#a1135a40aef2b6e46c99ad8f946c122d3',1,'XHXDeviceManager']]],
  ['getimagefromcolor_3a_1493',['getImageFromColor:',['../category_u_i_image_07_k_o_extension_08.html#afa4a20aa55aef0f83703bd88491a94be',1,'UIImage(KOExtension)']]],
  ['getimsi_1494',['getIMSI',['../interface_x_h_x_device_manager.html#a4c13d3e75be828f92a531ebc5c7b054e',1,'XHXDeviceManager']]],
  ['getiosversion_1495',['getIOSVersion',['../interface_x_h_x_device_manager.html#a2e073959d4af599e4f30d9328c10f88f',1,'XHXDeviceManager']]],
  ['getipaddress_1496',['getIPAddress',['../interface_x_h_x_device_manager.html#a085a7739cdce0d342f7ee650c0152820',1,'XHXDeviceManager']]],
  ['getiphoneuuid_1497',['getIphoneUUID',['../interface_x_h_x_device_manager.html#a3a151013ba67485638eeee32bf1b8522',1,'XHXDeviceManager']]],
  ['getuseragent_1498',['getUserAgent',['../interface_x_h_x_device_manager.html#af872cb3642699140b648159890e5e88f',1,'XHXDeviceManager']]],
  ['getversion_1499',['getVersion',['../interface_x_h_x_device_manager.html#a40d8dd0698975ba71440089a7439b2f1',1,'XHXDeviceManager']]],
  ['goback_1500',['goBack',['../interface_base_view_controller.html#a5d404912efe102f041e4a9dc367b5c79',1,'BaseViewController']]],
  ['greaterthanorequaltodate_3a_1501',['greaterThanOrEqualToDate:',['../category_n_s_date_07_date_format_08.html#ac75caa34b3c9bc7dccf1b01bb70d0869',1,'NSDate(DateFormat)']]]
];
