var indexSectionsWithContent =
{
  0: "_abcdefghijklmnopqrstuvwxy",
  1: "_bijkmnrstux",
  2: "bcjkmnrsux",
  3: "abcdfghijklmnoprstuvwx",
  4: "_abfhjktwx",
  5: "bgi",
  6: "abcdefghiklmnopqrstuvwxy",
  7: "abdfgiklmopqsuvx"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "properties",
  7: "defines"
};

var indexSectionLabels =
{
  0: "全部",
  1: "类",
  2: "文件",
  3: "函数",
  4: "变量",
  5: "类型定义",
  6: "属性",
  7: "宏定义"
};

