var interface_x_h_x_text_keyboard_manager =
[
    [ "addObserver:", "interface_x_h_x_text_keyboard_manager.html#a0799b3ebf30c4104e83d0c9a29b17454", null ],
    [ "convertRect:toView:", "interface_x_h_x_text_keyboard_manager.html#abf688329b06d2296b393fdef1a7e4d8c", null ],
    [ "removeObserver:", "interface_x_h_x_text_keyboard_manager.html#a4009a5f72bc1173e4b37949b5b0e2f65", null ],
    [ "UNAVAILABLE_ATTRIBUTE", "interface_x_h_x_text_keyboard_manager.html#aa8017a158e3aae47235dcfaf95bd947b", null ],
    [ "keyboardFrame", "interface_x_h_x_text_keyboard_manager.html#ab0735fbc05863c75b54e12fb9b59c440", null ],
    [ "keyboardView", "interface_x_h_x_text_keyboard_manager.html#af91fb8bd3f946a6f31b51e1f33879ef6", null ],
    [ "keyboardVisible", "interface_x_h_x_text_keyboard_manager.html#a300dd1d5b91c7b6c7c835673897dfe05", null ],
    [ "keyboardWindow", "interface_x_h_x_text_keyboard_manager.html#aa491574a2e8b5ab1352f8e5615ca6ef4", null ]
];