var interface_b_m_drag_cell_collection_view =
[
    [ "deleteDraggingItem", "interface_b_m_drag_cell_collection_view.html#a2bb07034e462b25ec32afe2f66e33cdf", null ],
    [ "dragMoveItemToIndexPath:", "interface_b_m_drag_cell_collection_view.html#abdbaa2415bb3dc04edb8df77df56f7c5", null ],
    [ "canDrag", "interface_b_m_drag_cell_collection_view.html#a8fb9e550e0d0a0cbf970605714345dd1", null ],
    [ "delegate", "interface_b_m_drag_cell_collection_view.html#a0d195d85fc67f302f75a56ea8fad37e2", null ],
    [ "dragCellAlpha", "interface_b_m_drag_cell_collection_view.html#aea3ec1078eda3e4d73405993635bac0d", null ],
    [ "dragZoomScale", "interface_b_m_drag_cell_collection_view.html#aa45e437c7d9d69c896b93edd9f91763f", null ],
    [ "minimumPressDuration", "interface_b_m_drag_cell_collection_view.html#a45e8b0e914c7241c534f487fb2f677c3", null ]
];