var _u_i_constants_8h =
[
    [ "kBackgroundColor", "_u_i_constants_8h.html#a43465a2a22046505549e4829619e4346", null ],
    [ "kDarkGrayTextColor", "_u_i_constants_8h.html#a54066c1a286473063317de23e37c3452", null ],
    [ "kDarkTextColor", "_u_i_constants_8h.html#abdc1a420f78eac5482495cfd8262b4d4", null ],
    [ "kDefDisableTintColor", "_u_i_constants_8h.html#a6a4f2fbc5409035b0ec080eba561031e", null ],
    [ "kDefOrangeColor", "_u_i_constants_8h.html#a3495f55afb78a1918db69cf840e11076", null ],
    [ "kDefRedColor", "_u_i_constants_8h.html#a3bf3d2fcacc0d6e61a931b937710b6c5", null ],
    [ "kDefTintColor", "_u_i_constants_8h.html#a9d317abe1dc63a5296866bbf7f9291dd", null ],
    [ "kDisableTintColor", "_u_i_constants_8h.html#a6eab6f86d8baf25921a050ad44b98acc", null ],
    [ "kGrayTextColor", "_u_i_constants_8h.html#a799e22b30ce6691c0ef82c3454100e91", null ],
    [ "kHighlightTintColor", "_u_i_constants_8h.html#a809a5bc09c4bcd96a7c38bb18a34415d", null ],
    [ "kLightLineColor", "_u_i_constants_8h.html#a75ad8716d7a2868174c56b0140215178", null ],
    [ "kNewBackgroundColor", "_u_i_constants_8h.html#a0eb667a8fef8e2fd0409c0667aaddc1e", null ],
    [ "kPicturePlacehold", "_u_i_constants_8h.html#a96d3d238033a3cf7381a3f64a7c49b9a", null ],
    [ "kTeacherAvatar", "_u_i_constants_8h.html#a81b706ab24ca0e16bb8489036539b171", null ],
    [ "kThemeColor", "_u_i_constants_8h.html#aab88703ee4f955d41b2fde4993c717b8", null ]
];