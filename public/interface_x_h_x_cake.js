var interface_x_h_x_cake =
[
    [ "bgPath", "interface_x_h_x_cake.html#aea19fb254a878b991d354c0cd49c85ab", null ],
    [ "endColor", "interface_x_h_x_cake.html#add9317236f0d2203308343d8071c6462", null ],
    [ "grLayer", "interface_x_h_x_cake.html#a2abf4d0e679a207e1f90814b08e67b8b", null ],
    [ "icon", "interface_x_h_x_cake.html#ad664922b59f7b2a939be31ce607f4fe7", null ],
    [ "imgView", "interface_x_h_x_cake.html#a044edaa8ed8c760f1d57fbd0a2c2a743", null ],
    [ "itemView", "interface_x_h_x_cake.html#ad9772016be2cc81e8d7cb1d471ce2823", null ],
    [ "lineLayer", "interface_x_h_x_cake.html#a76ed7385705ccdc9a05edcd96d5e620e", null ],
    [ "linePath", "interface_x_h_x_cake.html#a2fd9f7364f6361464b272b6fc9f80957", null ],
    [ "maskLayer", "interface_x_h_x_cake.html#a1d288a12a3e0d040d3106b2cb6b4abc7", null ],
    [ "name", "interface_x_h_x_cake.html#a1939721fae44bd2341293649ed403c80", null ],
    [ "progress", "interface_x_h_x_cake.html#ad2f75de3764eff1785423eebc9b9b8ec", null ],
    [ "startColor", "interface_x_h_x_cake.html#a69d819df27623ed9a12955d3af1fc37f", null ],
    [ "tintColor", "interface_x_h_x_cake.html#a22866ac7963ac47d1bab295e41e3a150", null ],
    [ "tintLayer", "interface_x_h_x_cake.html#a13cf0f9966e057eb17690d94a68f6c28", null ],
    [ "tintPath", "interface_x_h_x_cake.html#a6cc867d5c43e8b02e83952874eb876ac", null ],
    [ "tintWidth", "interface_x_h_x_cake.html#ab22b8507af70ef9364a7a09bf6c9604e", null ],
    [ "value", "interface_x_h_x_cake.html#af600285384d7f1af64d80dfd0f0b87cc", null ],
    [ "valuePath", "interface_x_h_x_cake.html#af56e454003dbe8bdbc2527327a3d2934", null ]
];