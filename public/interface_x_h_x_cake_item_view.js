var interface_x_h_x_cake_item_view =
[
    [ "darkMode", "interface_x_h_x_cake_item_view.html#a02ff71f5bbc9c142f6254b27f0580a95", null ],
    [ "defaultColor", "interface_x_h_x_cake_item_view.html#a47ccdc034ecd36bab23cb18df33828ce", null ],
    [ "dot", "interface_x_h_x_cake_item_view.html#a24695211dc1249b08395e052e6f05aa3", null ],
    [ "nameLabel", "interface_x_h_x_cake_item_view.html#ad5e0dd074d5643c5eedfb34a91dcc93d", null ],
    [ "picked", "interface_x_h_x_cake_item_view.html#afe590dd568714d72cec84198721e48a4", null ],
    [ "scoreLabel", "interface_x_h_x_cake_item_view.html#a1b9087a0802852ad0904680d945b672d", null ],
    [ "textColor", "interface_x_h_x_cake_item_view.html#ae21b213280f77b6ae12b5a63cafff70b", null ],
    [ "tintColor", "interface_x_h_x_cake_item_view.html#a916eb95a37b4b4b39d6bdfd4b50bd363", null ]
];