var searchData=
[
  ['checkphonenuminput_1452',['checkPhoneNumInput',['../category_n_s_string_07_k_o_extension_08.html#aedb6c7de0cc77da8301cf231f418e277',1,'NSString(KOExtension)']]],
  ['collectionview_3alayout_3abackgroundcolorforsection_3a_1453',['collectionView:layout:backgroundColorForSection:',['../protocol_j_h_collection_view_delegate_flow_layout-p.html#a6647c33a740eece33fc18c9fa92e4c04',1,'JHCollectionViewDelegateFlowLayout-p']]],
  ['colorwithhexstring_3a_1454',['colorWithHexString:',['../category_u_i_color_07_hex_08.html#ae687f69d612d4eafbdb037818cb920b7',1,'UIColor(Hex)']]],
  ['colorwithhexstring_3aalpha_3a_1455',['colorWithHexString:alpha:',['../category_u_i_color_07_hex_08.html#a55ab05f6ac4c655bbeaafb4c68927c51',1,'UIColor(Hex)']]],
  ['compressimagewithpixelsize_3a_1456',['compressImageWithPixelSize:',['../category_u_i_image_07_k_o_extension_08.html#a09c6aa85933736dab4bc5a398e06eddf',1,'UIImage(KOExtension)']]],
  ['configcontentwithtip_3a_1457',['configContentWithTip:',['../interface_beginner_tips_view.html#a6bd11891e2a5f42b04ccf717aedf379a',1,'BeginnerTipsView']]],
  ['convertrect_3atoview_3a_1458',['convertRect:toView:',['../interface_x_h_x_text_keyboard_manager.html#abf688329b06d2296b393fdef1a7e4d8c',1,'XHXTextKeyboardManager']]],
  ['createtableviewwithstyle_3a_1459',['createTableViewWithStyle:',['../interface_base_table_view_controller.html#a5365d7a603fe24b243e601bf03155481',1,'BaseTableViewController']]],
  ['creatqrcodewithdata_3a_1460',['creatQRCodeWithData:',['../interface_x_h_x_q_r_code_tools.html#af4b2fa46013c54070417f20d4d81049d',1,'XHXQRCodeTools']]],
  ['creatqrcodewithdata_3acenterimage_3a_1461',['creatQRCodeWithData:centerImage:',['../interface_x_h_x_q_r_code_tools.html#aae636aed99d415a3a97b398455bddee2',1,'XHXQRCodeTools']]],
  ['cropbywidthheightrate_3a_1462',['cropByWidthHeightRate:',['../category_u_i_image_07_k_o_extension_08.html#a486332d7fd239764da4be8f6bb8a2d19',1,'UIImage(KOExtension)']]],
  ['currentnavigationcontroller_1463',['currentNavigationController',['../interface_x_h_x_navigator.html#a5cf12ce028b8a0156c3cfd997f7c32b1',1,'XHXNavigator']]],
  ['currentviewcontroller_1464',['currentViewController',['../interface_x_h_x_navigator.html#a2386702d61e0ba8a8c3ded4c36aee1fc',1,'XHXNavigator']]]
];
