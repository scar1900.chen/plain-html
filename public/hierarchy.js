var hierarchy =
[
    [ "BaseCollectionViewController()", "category_base_collection_view_controller_07_08.html", null ],
    [ "BaseTableViewController()", "category_base_table_view_controller_07_08.html", null ],
    [ "BaseViewController()", "category_base_view_controller_07_08.html", null ],
    [ "BeginnerTipsManager()", "category_beginner_tips_manager_07_08.html", null ],
    [ "BeginnerTipsView()", "category_beginner_tips_view_07_08.html", null ],
    [ "BMDragCellCollectionView()", "category_b_m_drag_cell_collection_view_07_08.html", null ],
    [ "CAGradientLayer", null, [
      [ "XHXGradientLayer", "interface_x_h_x_gradient_layer.html", null ]
    ] ],
    [ "<CKBannerViewDelegate>", null, [
      [ "ScrollTabViewController", "interface_scroll_tab_view_controller.html", null ]
    ] ],
    [ "<CKDataModel>", null, [
      [ "<KKLAudioModel>", "protocol_k_k_l_audio_model-p.html", null ],
      [ "NSObject(DataModelExtension)", "category_n_s_object_07_data_model_extension_08.html", null ]
    ] ],
    [ "CKNavigationController", null, [
      [ "KONavigationController", "interface_k_o_navigation_controller.html", [
        [ "XHXNavigationController", "interface_x_h_x_navigation_controller.html", null ]
      ] ]
    ] ],
    [ "IndicatorPositon", "struct_indicator_positon.html", null ],
    [ "<KeyType>", null, [
      [ "XHXThreadSafeDictionary", "interface_x_h_x_thread_safe_dictionary.html", null ]
    ] ],
    [ "KKLAudioPlayer()", "category_k_k_l_audio_player_07_08.html", null ],
    [ "KKLScoreEvaluateVIew()", "category_k_k_l_score_evaluate_v_iew_07_08.html", null ],
    [ "KONavigationController()", "category_k_o_navigation_controller_07_08.html", null ],
    [ "MainTabTipView()", "category_main_tab_tip_view_07_08.html", null ],
    [ "MainTabTipWindowManager()", "category_main_tab_tip_window_manager_07_08.html", null ],
    [ "NSArray(XHXSafe)", "category_n_s_array_07_x_h_x_safe_08.html", null ],
    [ "NSDate(DateFormat)", "category_n_s_date_07_date_format_08.html", null ],
    [ "NSDate(TimeExtension)", "category_n_s_date_07_time_extension_08.html", null ],
    [ "NSDate(XHXExtention)", "category_n_s_date_07_x_h_x_extention_08.html", null ],
    [ "NSDictionary(Params)", "category_n_s_dictionary_07_params_08.html", null ],
    [ "NSJSONSerialization(RemovingNulls)", "category_n_s_j_s_o_n_serialization_07_removing_nulls_08.html", null ],
    [ "NSMutableArray(RemovingNulls)", "category_n_s_mutable_array_07_removing_nulls_08.html", null ],
    [ "NSMutableArray(XHXSafe)", "category_n_s_mutable_array_07_x_h_x_safe_08.html", null ],
    [ "NSMutableDictionary(RemovingNulls)", "category_n_s_mutable_dictionary_07_removing_nulls_08.html", null ],
    [ "NSMutableDictionary(XHXSafe)", "category_n_s_mutable_dictionary_07_x_h_x_safe_08.html", null ],
    [ "NSObject", null, [
      [ "_XHXTextKeyboardViewFrameObserver", "interface___x_h_x_text_keyboard_view_frame_observer.html", null ],
      [ "KKLAudioPlayer", "interface_k_k_l_audio_player.html", null ],
      [ "MainTabTipWindowManager", "interface_main_tab_tip_window_manager.html", null ],
      [ "TpCellClassType", "interface_tp_cell_class_type.html", null ],
      [ "XHXCake", "interface_x_h_x_cake.html", null ],
      [ "XHXCellClassType", "interface_x_h_x_cell_class_type.html", null ],
      [ "XHXDeviceManager", "interface_x_h_x_device_manager.html", null ],
      [ "XHXEnvUrlLoader", "interface_x_h_x_env_url_loader.html", null ],
      [ "XHXExtensionBlank", "interface_x_h_x_extension_blank.html", null ],
      [ "XHXKitAssets", "interface_x_h_x_kit_assets.html", null ],
      [ "XHXKitBaseFakeClass", "interface_x_h_x_kit_base_fake_class.html", null ],
      [ "XHXNavigator", "interface_x_h_x_navigator.html", null ],
      [ "XHXQRCodeTools", "interface_x_h_x_q_r_code_tools.html", null ],
      [ "XHXSafeData", "interface_x_h_x_safe_data.html", null ],
      [ "XHXSafeDataModel", "interface_x_h_x_safe_data_model.html", null ],
      [ "XHXSet", "interface_x_h_x_set.html", null ],
      [ "XHXTextKeyboardManager", "interface_x_h_x_text_keyboard_manager.html", null ],
      [ "XHXURLConfig", "interface_x_h_x_u_r_l_config.html", null ]
    ] ],
    [ "NSObject(JSContext)", "category_n_s_object_07_j_s_context_08.html", null ],
    [ "<NSObject>", null, [
      [ "<BeginnerTipsManagerDelegate>", "protocol_beginner_tips_manager_delegate-p.html", null ],
      [ "<MainTabTipWindowManagerDelegate>", "protocol_main_tab_tip_window_manager_delegate-p.html", null ],
      [ "<XHXSafeDataDelegate>", "protocol_x_h_x_safe_data_delegate-p.html", null ],
      [ "<XHXTabbarDelegate>", "protocol_x_h_x_tabbar_delegate-p.html", [
        [ "ScrollTabViewController", "interface_scroll_tab_view_controller.html", null ]
      ] ],
      [ "<XHXTextKeyboardObserver>", "protocol_x_h_x_text_keyboard_observer-p.html", null ]
    ] ],
    [ "NSProxy", null, [
      [ "XHXWeakProxy", "interface_x_h_x_weak_proxy.html", null ]
    ] ],
    [ "NSString(ABStr)", "category_n_s_string_07_a_b_str_08.html", null ],
    [ "NSString(AttributedString)", "category_n_s_string_07_attributed_string_08.html", null ],
    [ "NSString(JSONValue)", "category_n_s_string_07_j_s_o_n_value_08.html", null ],
    [ "NSString(KOExtension)", "category_n_s_string_07_k_o_extension_08.html", null ],
    [ "NSString(LabelWidthAndHeight)", "category_n_s_string_07_label_width_and_height_08.html", null ],
    [ "NSString(PasswordStrength)", "category_n_s_string_07_password_strength_08.html", null ],
    [ "NSString(TimeExtension)", "category_n_s_string_07_time_extension_08.html", null ],
    [ "NSString(URL)", "category_n_s_string_07_u_r_l_08.html", null ],
    [ "NSString(XHXTime)", "category_n_s_string_07_x_h_x_time_08.html", null ],
    [ "<ObjectType>", null, [
      [ "XHXThreadSafeArray", "interface_x_h_x_thread_safe_array.html", null ],
      [ "XHXThreadSafeDictionary", "interface_x_h_x_thread_safe_dictionary.html", null ]
    ] ],
    [ "<POPAnimationDelegate>", null, [
      [ "UIView(FadeInFadeOut)", "category_u_i_view_07_fade_in_fade_out_08.html", null ]
    ] ],
    [ "UIBarButtonItem(XHXStyle)", "category_u_i_bar_button_item_07_x_h_x_style_08.html", null ],
    [ "UIButton", null, [
      [ "XHXButton", "interface_x_h_x_button.html", null ],
      [ "XHXSimButton", "interface_x_h_x_sim_button.html", null ],
      [ "XHXTabItem", "interface_x_h_x_tab_item.html", null ]
    ] ],
    [ "UIButton(XHXStyle)", "category_u_i_button_07_x_h_x_style_08.html", null ],
    [ "UICollectionReusableView", null, [
      [ "JHCollectionReusableView", "interface_j_h_collection_reusable_view.html", null ]
    ] ],
    [ "UICollectionView", null, [
      [ "BMDragCellCollectionView", "interface_b_m_drag_cell_collection_view.html", null ]
    ] ],
    [ "UICollectionView(BMDragCellCollectionViewRect)", "category_u_i_collection_view_07_b_m_drag_cell_collection_view_rect_08.html", null ],
    [ "UICollectionView(BMRect)", "category_u_i_collection_view_07_b_m_rect_08.html", null ],
    [ "<UICollectionViewDataSource>", null, [
      [ "BaseCollectionViewController", "interface_base_collection_view_controller.html", null ]
    ] ],
    [ "<UICollectionViewDelegate>", null, [
      [ "BaseCollectionViewController", "interface_base_collection_view_controller.html", null ]
    ] ],
    [ "<UICollectionViewDelegateFlowLayout>", null, [
      [ "BaseCollectionViewController", "interface_base_collection_view_controller.html", null ],
      [ "<BMDragCellCollectionViewDelegate>", "protocol_b_m_drag_cell_collection_view_delegate-p.html", null ],
      [ "<JHCollectionViewDelegateFlowLayout>", "protocol_j_h_collection_view_delegate_flow_layout-p.html", null ]
    ] ],
    [ "UICollectionViewFlowLayout", null, [
      [ "JHCollectionViewFlowLayout", "interface_j_h_collection_view_flow_layout.html", null ]
    ] ],
    [ "UICollectionViewLayoutAttributes", null, [
      [ "JHCollectionViewLayoutAttributes", "interface_j_h_collection_view_layout_attributes.html", null ]
    ] ],
    [ "UIColor(Hex)", "category_u_i_color_07_hex_08.html", null ],
    [ "UIControl", null, [
      [ "RadioView", "interface_radio_view.html", null ]
    ] ],
    [ "UIFont(KOExtension)", "category_u_i_font_07_k_o_extension_08.html", null ],
    [ "UIImage(KOExtension)", "category_u_i_image_07_k_o_extension_08.html", null ],
    [ "UIImage(XHXCommonResource)", "category_u_i_image_07_x_h_x_common_resource_08.html", null ],
    [ "UIImage(XHXExtension)", "category_u_i_image_07_x_h_x_extension_08.html", null ],
    [ "UIImageView(KOWebImage)", "category_u_i_image_view_07_k_o_web_image_08.html", null ],
    [ "UILabel(Extension)", "category_u_i_label_07_extension_08.html", null ],
    [ "UILabel(Vertical)", "category_u_i_label_07_vertical_08.html", null ],
    [ "UILabel(XHXExtension)", "category_u_i_label_07_x_h_x_extension_08.html", null ],
    [ "UINavigationBar(KONavigationController)", "category_u_i_navigation_bar_07_k_o_navigation_controller_08.html", null ],
    [ "<UIPickerViewDelegate>", null, [
      [ "<XHXPickerViewDelegate>", "protocol_x_h_x_picker_view_delegate-p.html", null ]
    ] ],
    [ "UIResponder(KKLCustomSettings)", "category_u_i_responder_07_k_k_l_custom_settings_08.html", null ],
    [ "UITabBarItem(DotView)", "category_u_i_tab_bar_item_07_dot_view_08.html", null ],
    [ "UITableView(TpCellClass)", "category_u_i_table_view_07_tp_cell_class_08.html", null ],
    [ "UITableView(XHXCellClass)", "category_u_i_table_view_07_x_h_x_cell_class_08.html", null ],
    [ "UITableViewCell", null, [
      [ "KOTableViewCell", "interface_k_o_table_view_cell.html", [
        [ "BackgroundTableViewCell", "interface_background_table_view_cell.html", null ],
        [ "KOCustomeCell", "interface_k_o_custome_cell.html", null ]
      ] ]
    ] ],
    [ "<UITableViewDataSource>", null, [
      [ "BaseTableViewController", "interface_base_table_view_controller.html", null ]
    ] ],
    [ "<UITableViewDelegate>", null, [
      [ "BaseTableViewController", "interface_base_table_view_controller.html", null ]
    ] ],
    [ "UITextView(XHXPlaceholder)", "category_u_i_text_view_07_x_h_x_placeholder_08.html", null ],
    [ "UIView", null, [
      [ "BeginnerTipsManager", "interface_beginner_tips_manager.html", null ],
      [ "BeginnerTipsView", "interface_beginner_tips_view.html", null ],
      [ "KKLScoreEvaluateVIew", "interface_k_k_l_score_evaluate_v_iew.html", null ],
      [ "MainTabTipView", "interface_main_tab_tip_view.html", null ],
      [ "XHXBlankView", "interface_x_h_x_blank_view.html", [
        [ "XHXActivityBlankView", "interface_x_h_x_activity_blank_view.html", null ],
        [ "XHXImageBlankView", "interface_x_h_x_image_blank_view.html", [
          [ "XHXTextBlankView", "interface_x_h_x_text_blank_view.html", [
            [ "XHXTextLoadingBlankView", "interface_x_h_x_text_loading_blank_view.html", null ]
          ] ]
        ] ]
      ] ],
      [ "XHXCakeItemView", "interface_x_h_x_cake_item_view.html", null ],
      [ "XHXCakeView", "interface_x_h_x_cake_view.html", null ],
      [ "XHXGradientView", "interface_x_h_x_gradient_view.html", null ],
      [ "XHXPickerView", "interface_x_h_x_picker_view.html", null ],
      [ "XHXTabbar", "interface_x_h_x_tabbar.html", [
        [ "KOTabBar", "interface_k_o_tab_bar.html", null ]
      ] ]
    ] ],
    [ "UIView(CKLine)", "category_u_i_view_07_c_k_line_08.html", null ],
    [ "UIView(Extension)", "category_u_i_view_07_extension_08.html", null ],
    [ "UIView(KKLBadgeView)", "category_u_i_view_07_k_k_l_badge_view_08.html", null ],
    [ "UIView(KOToast)", "category_u_i_view_07_k_o_toast_08.html", null ],
    [ "UIView(SetRect)", "category_u_i_view_07_set_rect_08.html", null ],
    [ "UIView(XHXLine)", "category_u_i_view_07_x_h_x_line_08.html", null ],
    [ "UIView(XHXRect)", "category_u_i_view_07_x_h_x_rect_08.html", null ],
    [ "UIViewController", null, [
      [ "BaseViewController", "interface_base_view_controller.html", [
        [ "BaseCollectionViewController", "interface_base_collection_view_controller.html", null ],
        [ "BaseTableViewController", "interface_base_table_view_controller.html", null ],
        [ "ScrollTabViewController", "interface_scroll_tab_view_controller.html", null ]
      ] ]
    ] ],
    [ "UIViewController(KKLCustomSettings)", "category_u_i_view_controller_07_k_k_l_custom_settings_08.html", null ],
    [ "UIViewController(KONavigationController)", "category_u_i_view_controller_07_k_o_navigation_controller_08.html", null ],
    [ "XHXCakeView()", "category_x_h_x_cake_view_07_08.html", null ],
    [ "XHXGatewayTask", null, [
      [ "URLFetchTask", "interface_u_r_l_fetch_task.html", null ]
    ] ],
    [ "XHXGradientView()", "category_x_h_x_gradient_view_07_08.html", null ],
    [ "XHXNavigationController()", "category_x_h_x_navigation_controller_07_08.html", null ],
    [ "XHXPickerView()", "category_x_h_x_picker_view_07_08.html", null ],
    [ "XHXSafeData()", "category_x_h_x_safe_data_07_08.html", null ],
    [ "XHXSet()", "category_x_h_x_set_07_08.html", null ],
    [ "XHXTabbar()", "category_x_h_x_tabbar_07_08.html", null ],
    [ "XHXTabItem()", "category_x_h_x_tab_item_07_08.html", null ],
    [ "XHXTextBlankView()", "category_x_h_x_text_blank_view_07_08.html", null ],
    [ "XHXTextKeyboardTransition", "struct_x_h_x_text_keyboard_transition.html", null ],
    [ "XHXThreadSafeArray()", "category_x_h_x_thread_safe_array_07_08.html", null ],
    [ "XHXThreadSafeDictionary()", "category_x_h_x_thread_safe_dictionary_07_08.html", null ]
];